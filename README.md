# Skinny Widgets Tabs for Antd4 Theme


tabs element

```
npm i sk-tabs sk-tabs-antd4 --save
```

then add the following to your html

```html
<sk-config
    theme="antd4"
    base-path="/node_modules/sk-core/src"
    theme-path="/node_modules/sk-theme-antd4"
></sk-config>
<sk-tabs id="tabs">
    <sk-tab title="foo">
        some foo tab contents
    </sk-tab>
    <sk-tab title="bar">
        some bar tab contents
    </sk-tab>
    <sk-tab title="baz">
        some baz tab contents
    </sk-tab>
</sk-tabs>
<script type="module">
    import { SkTabs } from './node_modules/sk-tabs/index.js';

    customElements.define('sk-tabs', SkTabs);
</script>
```

#### attributes

**disabled** - all tabs disabled


#### template

id: SkTabsTpl

### sk-sl-tabs element

tabs that use slots technology

```html
<sk-config
    theme="antd4"
    base-path="/node_modules/sk-core/src"
    theme-path="/node_modules/sk-theme-antd4"
></sk-config>
<sk-sl-tabs id="slTabs">
  <h2 slot="title" disabled>Title</h2>
  <section>content panel 1</section>
  <h2 slot="title" selected>Title 2</h2>
  <section>content panel 2</section>
  <h2 slot="title">Title 3</h2>
  <section>content panel 3</section>
</sk-sl-tabs>
```

It is also possible to use **sk-tab** subelements, but this will add some prerendering
to slotted formatting

```html
<sk-sl-tabs id="slTabs">
    <sk-tab title="Tab 1">
        tab 1 contents
    </sk-tab>
    <sk-tab title="Tab 2" open>
        tab 2 contents
    </sk-tab>
    <sk-tab title="Tab 3" disabled>
        tab 3 contents
    </sk-tab>
</sk-sl-tabs>
```

#### attributes

**disabled** - all tabs disabled

#### template

id: SkSlTabsTpl
