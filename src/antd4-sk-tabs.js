
import { AntdSkTabs }  from '../../sk-tabs-antd/src/antd-sk-tabs.js';

import { OPEN_AN, TITLE_AN } from "../../sk-tabs/src/sk-tabs.js";

import { DISABLED_AN } from "../../sk-core/src/sk-element.js";

export class Antd4SkTabs extends AntdSkTabs {

    get prefix() {
        return 'antd4';
    }

    get titleBarEl() {
        if (! this._tabsTitleBarEl) {
            this._tabsTitleBarEl = this.tabsEl.querySelector('.ant-tabs-nav .ant-tabs-nav-list');
        }
        return this._tabsTitleBarEl;
    }

    set titleBarEl(el) {
        this._tabsTitleBarEl = el;
    }

    renderTabs(tabEls) {
        if (this.comp.isTplSlotted) {
            this.renderSlottedTabs(tabEls);
        } else {
            //this.tabsEl.insertAdjacentHTML('beforeend', this.contentsState || this.comp.contentsState);
            let tabs = tabEls || this.tabsEl.querySelectorAll(this.comp.tabSl);
            let num = 1;
            this.tabs = {};
            for (let tab of tabs) {
                let isOpen = tab.hasAttribute(OPEN_AN);
                let title = tab.getAttribute(TITLE_AN) ? tab.getAttribute(TITLE_AN) : '';
                this.titleBarEl.insertAdjacentHTML('beforeend', `
                    <div data-tab="tabs-${num}" aria-disabled="false" aria-selected="true" class="ant-tabs-tab ${isOpen ? 'ant-tabs-tab-active' : ''} ${tab.hasAttribute(DISABLED_AN) ? 'ant-tabs-tab-disabled' : ''}"
                         role="tab" ${isOpen ? 'open' : ''}>${title}
                    </div>
                `);
                this.tabsContentEl.insertAdjacentHTML('beforeend', `
                    <div data-tab="tabs-${num}" aria-hidden="false" class="ant-tabs-tabpane ${isOpen ? 'ant-tabs-tabpane-active' : ''}" 
                        ${! isOpen ? 'style="display: none;"' : ''} role="tabpanel">
                        ${tab.outerHTML}
                    </div>
                `);
                if (isOpen) {
                    this.inkBarToTab(num);
                }
                this.removeEl(tab);
        
                this.tabsContentEl.querySelectorAll('div').forEach(tab => {
                    if (tab.getAttribute('data-tab') === 'tabs-' + num ) {
                        this.tabs['tabs-' + num] = tab;
                    }
                });
                num++;
            }
            this.titleBarEl.insertAdjacentHTML('beforeend', `
                <div class="ant-tabs-ink-bar ant-tabs-ink-bar-animated"
                     style="display: block; transform: translate3d(0px, 0px, 0px); width: 66px;"></div>
            `);
        }
    }

    inkBarToTab(tab) {
        let activeTab = this.titleBarEl.querySelector(`[data-tab=${tab}]`);
        activeTab.classList.add('ant-tabs-tab-active');
        let rect = activeTab.getBoundingClientRect();
        let padding = window.getComputedStyle(activeTab, null).getPropertyValue('padding-left');
        this.inkBarEl.style.transform = `translate3d(${rect.left - parseFloat(padding)}px, 0px, 0px)`;
    }

}
