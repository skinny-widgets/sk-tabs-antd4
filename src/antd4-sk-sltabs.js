
import { SkSltabsImpl }  from '../../sk-tabs/src/impl/sk-sltabs-impl.js';

export class Antd4SkSltabs extends SkSltabsImpl {

    get prefix() {
        return 'antd4';
    }

    get suffix() {
        return 'sltabs';
    }

}
