
import { SkTabImpl }  from '../../sk-tabs/src/impl/sk-tab-impl.js';

export class Antd4SkTab extends SkTabImpl {

    get prefix() {
        return 'antd4';
    }

    get suffix() {
        return 'tab';
    }

}
